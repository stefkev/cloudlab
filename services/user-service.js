var bcrypt = require('bcrypt');
var User = require('../models/user').User;

var gridfs = require("../app").GridFS;


exports.addUser = function(user, next) {
  bcrypt.hash(user.password, 10, function(err, hash) {
    if (err) {
      return next(err);
    }
    user.password = hash;

    var newUser = new User(user);
    
    newUser.save(function(err) {
      if (err) {
        return next(err);
      }
      next(null);
    });
  });
};

exports.insertPicture = function (filPath) {}

exports.deleteUser = function(user, next) {
  
}

exports.editUser = function(user, next) {
  
}

exports.findEmail = function(email, next) {
  User.findOne({email: email}, function(err, email) {
    next(err, email);    
  });
};

exports.findUser = function(userName, next) {
  User.findOne({userName: userName}, function(err, user) {
    next(err, user);    
  });
};

exports.listUsers = function(userName, next) {
  User.find({}, function(err, user) {
    next(err, user);
  });
};


exports.findByEmail = function (email, next) {
    User.findOne({email:email}, function (err, data) {
        next(data);
    })
}

exports.findByUsername = function (username, next) {
    User.findOne({username: username}, function (err, data) {
        next(data);
    })
}
exports.findUser_id = function(_id, next) {
  User.findOne({_id: _id}, function(err, _id) {
    next(err, _id);    
  });
};