var im = require('imagemagick');
var fs = require('fs');
var path = require('path');
var os =require('os');

var grid = require('../app').GridFS;



exports.generatePicture = function(filePath, width, cb){
    var parsed = path.parse(filePath);
    console.log(parsed);
    var tmpDir = os.tmpdir();

    var newfilename = '';
    if (width<=10){
        newfilename = parsed.name +'_small'+parsed.ext;
    }else {
        newfilename = parsed.name + '_large'+parsed.ext;
    }
    im.resize({
        srcPath: parsed.dir + '/'+ parsed.base,
        dstPath: tmpDir + '/'+ newfilename,
        width: width
    }, function (err, stdout, stderr) {
        if (err) throw err;
        cb({path: tmpDir +'/' + newfilename, name: newfilename});
    });
};

exports.getPicture = function (res, picId) {
    var readStream = grid.createReadStream({_id: picId});
    readStream.pipe(res);
}

exports.savePicture = function (filePath, cb) {
    var writeStream = grid.createWriteStream()
    writeStream.on('close', function (data) {
        return cb(data);
    })
}

exports.deletePicture = function (smallPicId, largePicId, cb) {
    grid.remove({
        _id: smallPicId
    }, function (errSmall) {
        if(errSmall) return cb(errSmall);
        grid.remove({_id: largePicId}, function (errBig) {
            if(errBig) return cb(errBig)
            cb();
        })
    })
}