var fs = require('fs');
var app = require('../app');
var Project = require('../models/project').Project;
var Stem = require('../models/stem').Stem;
var Stream = require("stream");

exports.addProject = function (user, project, next) {
    project.creator = user._id;
    var newProject = new Project({
        projectName: project.name,
        creator: project.creator,
        description: project.description,
        created: project.date,
        hidden: true,
        coverArt: project.coverArt
    });

    newProject.save(function (err, data) {
        if (err) {
            return next(err);
        }
        return next(null, data);
    });
};

exports.findAll = function (user, next) {
    Project.findAll(user, function (err, data) {
        if (err) return next(err);
        return next(null, data);
    });
};

exports.uploadStem = function (stem, projectID, next) {
    var writestream = app.GridFS.createWriteStream({
        filename: stem.name
    });
    writestream.on('close', function (file) {
        Stem.save({name:file.name, reID: file._id}, function (err, stemData) {
            Project.save({_id: projectID, Stem:[{_id:stemData._id}]}, function (err, projData) {
                if(err) return next(err);
                return next();
            });
        });
    });
    fs.createReadStream(stem.path).pipe(writestream);

};

exports.readStem = function (id) {
    var readstream = app.GridFS.createReadStream({_id: id});
    var dest = new Stream();
    dest.writable = true;
    readstream.pipe(dest);

};

exports.addComment = function (project, user, comment, next) {

    comment.project = project._id;
    comment.user = user._id;
    var newProject = new Project({
        project: comment.project,
        body: comment.body,
        user: comment.user,
        date: comment.date
    });

    newProject.save(function (err) {
        if (err) {
            return next(err);
        }
        return next(null);
    });
};

exports.findName = function (name, next) {
    Project.findOne({name: name.toLowerCase()}, function (err, project) {
        next(err, project);
    });
};

exports.findId = function (_id, next) {
    Project.findOne({_id: _id}, function (err, _id) {
        next(err, _id);
    });
};

exports.findCreator = function (creator, next) {
    Project.findOne({creator: creator}, function (err, project) {
        next(err, project);
    });
};

exports.list = function (id, next) {
    Project.find({creator: id}).sort({date: 'desc'}).exec(function (err, project) {
        return next(err, project);
    });
};

exports.getProject = function(id, next){
    return Project.findOne({_id: id}).populate('stems').exec(function(err, data){
        if(err) return next(err);
        return next(null, data);
    });
};

exports.getCountLike = function(id, next){
    return Project.aggregate([{$match: {_id: id}}, {likes:{$size: '$likes'}}]).exec(function(err, data){
        if(err) return next(err);
        return next(null, data);
    });
};
exports.getCountView = function(id, next){
    return Project.aggregate([{$match: {_id: id}}, {views:{$size: 'views'}}]).exec(function(err, data){
        if(err) return cb(err);
        return cb(null, data);
    });
};
exports.didILike = function(id, userID, next){
    return Project.findOne({_id: id, user: {$in: 'likes'}}).exec(function(err, data){
        if(err) return next(err);
        return next(null, data);
    });
};
