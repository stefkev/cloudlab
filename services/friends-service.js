var neo4j = require('neo4j-driver').v1;
var config = require('../config');

var driver = neo4j.driver(config.neo4jUri, neo4j.auth.basic(config.neo4jUser, config.neo4jPass));

driver.onCompleted = function () {
    console.log("neo4j driver connected successfully")
};



driver.onError = function (error) {
    throw err;
};

function createPerson(dbID, fullName) {

}

function getPerson(dbID) {

}

function getFriends(dbID) {

}
function addFriend(dbID) {

}
function removeFriend(dbID) {

}

exports.createPerson = createPerson;
exports.getPerson = getPerson;
exports.getFriends = getFriends;
exports.addFriend = addFriend;
exports.removeFriend = removeFriend;

// process.on('SIGINT', function (code) {
//     driver.close();
//     console.log("neo4j driver closed");
// })
// process.on('exit', function (code) {
//     driver.close();
//     console.log("neo4j driver closed");
// })