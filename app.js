//just to instatiate neo4j conn

var http = require("http");
var express = require('express');
var bluebird = require('bluebird');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Grid = require('gridfs-stream');
var passport = require('passport');
var expressSession = require('express-session');
var flash = require('connect-flash');
var connectMongo = require('connect-mongo');
var MongoStore = connectMongo(expressSession);
var config = require('./config');
// var formidable = require('express-formidable');
var multipart = require('connect-multiparty');


mongoose.connect(config.mongoUri, {promiseLibrary: bluebird});

var GridFS = Grid(mongoose.connection.db, mongoose.mongo);
exports.GridFS = GridFS;


// require('./services/friends-service')
var routes = require('./routes/index');
var users = require('./routes/users');
var home = require('./routes/home');
var project = require("./routes/project");
// var trends = require('./routes/trends');



var passportConfig = require('./auth/passport-config');
var restrict = require('./auth/restrict');
passportConfig();


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(multipart());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(expressSession(
    {
        secret: 'Gilgamesh',
        saveUninitialized: false,
        resave: true,
        store: new MongoStore({
           mongooseConnection: mongoose.connection 
        })
    }
));

app.use(passport.initialize());
app.use(passport.session());

// app.use('/', routes);
app.use('/users', users);
// app.use('/home', home);
app.use('/project', project);
app.get('/*', function (req, res) {
    res.render('index');
});




// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// prints stacktrace
if (app.get('env') === 'development') {
    // app.use(function(err, req, res, next) {
    //     res.status(err.status || 500);
    //     res.render('error', {
    //         message: err.message,
    //         error: err
    //     });
    // });
}

// production error handler
// no stacktraces
app.use(function(err, req, res, next) {
    // res.status(err.status || 500);
    // res.render('error', {
    //     message: err.message,
    //     error: {}
    // });
});
module.exports = app;
