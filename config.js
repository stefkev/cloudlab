var config = {};

config.mongoUri = 'mongodb://localhost:27017/rtr';
config.cookieMaxAge = 30 * 24 * 3600 * 1000;

config.neo4jUri = 'bolt://localhost:7474';
config.neo4jUser = '';
config.neo4jPass = '';


module.exports = config;
