var express = require('express');
var router = express.Router();
var passport = require('passport');
var userService = require('../services/user-service');
var pictureService = require('../services/picture-service');
var config = require('../config');
var util = require('util');
var fs = require('fs');
var path = require('path');

var grid = require('../app').GridFS;
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;


router.get('/checkUsername', function (req, res) {
    var username = req.query.userName;
    userService.findByUsername(username, function (data) {
        if (data == undefined || data == null) {
            return res.json({exists: false});
        }
        return res.json({exists: true});
    });
});

router.get('/checkEmail', function (req, res) {
    var email = req.query.email;
    return userService.findByEmail(email, function (data) {
        if (data == undefined) {
            return res.json({exists: false});
        }
        return res.json({exists: true});
    });
});


router.get('/', function (req, res) {
    var user = req.session.passport.user;
    if(user){
        userService.findEmail(user, function (err, data) {
            if (err) {
                return res.send(err);
            }
            return res.json(data);
        });
    }else {
        res.json({data:null});
    }


});


router.get('/checkAuth', function (req, res) {
    if (req.user) {
        return res.json({loggedIn: true});
    }
    return res.json({loggedIn: false});
});


router.post('/create', function (req, res) {
    return userService.addUser(req.body, function (err) {
        if (err) return res.json({err: err});
        return res.json({registered: true});
    });
});

router.post('/login', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
        if (err) { return next(err); }
        if (!user) { return res.json({loggedIn: false}); }
        return req.logIn(user, function(err) {
            if (err) { return next(err); }
            return res.json({loggedIn:true});
        });
    })(req, res, next);
});

router.get('/logout', function (req, res) {
    req.session.destroy(function (err) {
        if(err) return res.json({loggedOut: false});
        return res.json({loggedOut: true});
    });

});

router.get('/getLargePicture', function (req, res) {
    var picID = req.query.largePicId;
    pictureService.getPicture(res, picID);
});
router.get('/getSmallPicture', function (req, res) {
    var picID = req.query.smallPicId;
    pictureService.getPicture(res, picID)
})
router.post('/insertProfilePicture', function (req, res) {
    
})
router.post('/deleteProfilePicture', function (req,res) {
    var smallPic = req.query.smallPicId;
    var largePic = req.query.largePicId;
    pictureService.deletePicture(smallPic, largePic, function (err) {
        if(err){
            res.json({err:'Error occurred on picture deleter'})
        }
        res.json({deleted:true})
    })
})


module.exports = router;
