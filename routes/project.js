var express = require('express');
var router = express.Router();
var gridfs = require("../app.js").gridfs;

var projectService = require("../services/project-service");

router.get('/', function (req, res) {
    if(req.query.projectID){
       return projectService.findId(req.query.projectID, function (err, data) {
            if(err) return res.json(err);
            return res.json(data);
        });
    }
    var project = {
        name: 'Untitled project',
        description: 'No description for project',
        coverArt: "/images/project_pic.png"
    };
    return projectService.addProject(req.user, project, function (err, data) {
        req.session.projectID = data._id;
        if(err) return res.json(err);
        return res.json(data);
    });
});


router.get('/listProjects', function (req, res) {
    projectService.findAll(req.user, function (err, data) {
        if(err) return res.json(err);
        return res.json(data);
    });
});

router.post('/updateProject', function (req, res) {
    projectService.addProject(req.user, req.body.project, function (err, data) {
        if(err) return res.json(err);
        return res.json(data);
    });
});

router.get("/list", function (req, res) {
    projectService.list(req.user._id, function (err, data) {
        console.log(data);
        if(err) return res.json(err);
        return res.json(data);
    });
});

router.get('/getProject', function(req, res){
    project.list(req.user._id, function(err, data){
        if(err) return res.json(err);
        return res.json(data);
    });
});
router.get('/getStem', function (req, res) {
    projectService.readStem(req.query.id).pipe(res);
});

router.post('/saveStem', function (req, res) {
    var writestream = gridfs.createWriteStream();
    writestream.on('close', function(file){
        res.end();
    });
    req.pipe(writestream);
});


router.post('/compileSong', function (req, res) {

});
module.exports = router;
