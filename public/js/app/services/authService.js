angular.module('app')
    .service('AuthService', function ($http, $q, $log) {
        function isLoggedIn() {
            var request = $http({
                method: 'GET',
                url: '/users/checkAuth'
            });
            return request.then(handleSuccess, handleError);
        }

        function getUser() {
            var request = $http({
                method: 'GET',
                url: '/users'
            });
            return request.then(handleSuccess, handleError);
        }

        function logout() {
            var request = $http({
                method: 'GET',
                url: '/users/logout'
            });
            return request.then(handleSuccess, handleError);
        }


        function login(email, password) {
            var request = $http({
                method: 'POST',
                url: '/users/login',
                data: {
                    email: email,
                    password: password
                }
            });
            return request.then(handleSuccess, handleError);
        }


        function checkUsername(username) {
            var request = $http({
                method: 'GET',
                url: '/users/checkUsername',
                params: {
                    userName: username
                }
            });
            return request.then(handleSuccess, handleError);
        }


        function checkEmail(email) {
            var request = $http({
                method: 'GET',
                url: '/users/checkEmail',
                params: {
                    email: email
                }
            });
            return request.then(handleSuccess, handleError);
        }


        function register(data) {
            var request = $http({
                method: 'POST',
                url: '/users/create',
                data: data
            });
            return request.then(handleSuccess, handleError);
        }

        function handleSuccess(response) {
            return response.data;
        }


        function handleError(response) {
            if (
                !angular.isObject(response.data) ||
                !response.data.message
            ) {
                return ( $q.reject("An unknown error occurred.") );
            }
            // Otherwise, use expected error message.
            return ( $q.reject(response.data.message) );
        }

        return {
            isLoggedIn: isLoggedIn,
            getUser: getUser,
            logout: logout,
            login: login,
            checkEmail: checkEmail,
            checkUsername: checkUsername,
            register: register
        }
    });
