angular.module('app')
    .service('EditorService', function(Upload, $http, $q, $log) {
        return {
            uploadStem: uploadStem,
            getProject: getProject,
            deleteStem: deleteStem,
            listUserProject: listUserProject,
            getProject: getProject,
            getStem: getStem,
            createProject: createProject,
            compileSong: compileSong
        };


        function uploadStem(file, projectID) {
            Upload.upload({
                url: '/project/saveStem',
                data: {
                    file: file,
                    'id': projectID
                }
            }).then(function(resp) {
                handleSuccess(resp);
            }, function(resp) {
                handleError(resp);
            });
        }

        function createProject() {
            var request = $http({
                method: 'GET',
                url: '/project/create'
            });
            return request.then(handleSuccess, handleError);
        }

        function getProject(projectID) {
            var request = $http({
                method: 'GET',
                url: '/project',
                params: {
                    projectID: projectID
                }
            });
            return request.then(handleSuccess, handleError);
        }

        function listUserProject() {
            var request = $http({
                method: 'GET',
                url: '/project/list'
            });
            return request.then(handleSuccess, handleError);
        }

        function deleteStem(data) {
            return $http.get('/project/deleteStem', data)
                .then(function(response) {
                    handleSuccess(response);
                }, function(response) {
                    handleError(response);
                });
        }

        function getStem() {
            return $http.get('/project/getStem', data)
                .then(function(response) {
                    handleSuccess(response);
                }, function(response) {
                    handleError(response);
                });
        }

        function compileSong(file, projectID) {
            Upload.upload({
                url: '/project/compileSong',
                data: {
                    file: file,
                    'id': projectID
                }
            }).then(function(resp) {
                handleSuccess(resp);
            }, function(resp) {
                handleError(resp);
            });
        }

        function handleSuccess(response) {
            return response.data;
        }

        function handleError(response) {
            if (!angular.isObject(response.data) ||
                !response.data.message
            ) {
                return ($q.reject("An unknown error occurred."));
            }
            // Otherwise, use expected error message.
            return ($q.reject(response.data.message));
        }
    });
