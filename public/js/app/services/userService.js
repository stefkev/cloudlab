angular.module('app')
    .service('UserService', function ($http, Upload) {
        return {
            updateData: function (data) {
                return $http.post('/users/update', data)
                    .then(function (response) {
                        return response.data;
                    });
            },
            updatePassword: function (password) {
                return $http.post('/users/updatePass', {password: password})
                    .then(function (response) {
                        return response.data;
                    });
            },
            updateProfilePic: function (profilePic) {
                Upload.upload({
                    url: $scope.uploadStemUrl,
                    data: {
                        file: file,
                        'id': 'test'
                    }
                }).then(function(resp) {
                    console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                    handleSuccess(resp);
                }, function(resp) {
                    console.log('Error status: ' + resp.status);
                    handleError(resp);
                }, function(evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            }


        };
    });

