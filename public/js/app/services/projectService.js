angular.module('app')
    .service('EditorService', function(Upload, $http, $q, $log) {
        return {
            uploadStem: uploadStem,
            getProject: getProject,
            deleteStem: deleteStem,
            getStem: getStem,
            compileSong: compileSong
        };


        function uploadStem(file, projectID) {
            Upload.upload({
                url: '/project/saveStem',
                data: {
                    file: file,
                    'id': projectID
                }
            }).then(function(resp) {
                handleSuccess(resp);
            }, function(resp) {
                handleError(resp);
            });
        }

        function listUserProject(){
            return $http.get('/project/listProjects')
                .then(function(response){
                    return response.data;
                });
        }

        function getProject(projectID) {
            if(projectID){
                return $http.get('/project?projectID='+projectID)
                    .then(function(response) {
                        return response.data;
                    });
            }
            return $http.get('/project')
                .then(function(response) {
                    return response.data;
                });

        }

        function deleteStem(data) {
            return $http.get('/project/deleteStem', data)
                .then(function(response) {
                    return response.data;
                });
        }

        function getStem() {
            return $http.get('/project/getStem', data)
                .then(function(response){
                    return response.data;
                });
        }

        function compileSong(file, projectID) {
            Upload.upload({
                url: '/project/compileSong',
                data: {
                    file: file,
                    'id': projectID
                }
            }).then(function(resp) {
                handleSuccess(resp);
            }, function(resp) {
                handleError(resp);
            });
        }

        function handleSuccess(response) {
            return response.data;
        }

        function handleError(response) {
            if (!angular.isObject(response.data) ||
                !response.data.message
            ) {
                return ($q.reject("An unknown error occurred."));
            }
            // Otherwise, use expected error message.
            return ($q.reject(response.data.message));
        }
    });
