angular.module('app')
    .controller('Home', ['$scope', 'EditorService', 'AuthService', 'UserService', '$state', '$log', function ($scope, EditorService, AuthService, UserService, $state, $log) {
        AuthService.isLoggedIn()
            .then(function (respData) {
                if (respData.loggedIn == false) $state.go('login');
            });
        AuthService.getUser()
            .then(function (respData) {
                $scope.userData = respData;
            });
        EditorService.listUserProject()
            .then(function (respData){
                $scope.projects= respData;
            });
    }]);
