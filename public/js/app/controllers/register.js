angular.module('app')
    .controller('Register', ['$scope', 'AuthService', '$log', '$state', function ($scope, AuthService, $log, $state) {
        AuthService.isLoggedIn()
            .then(function (respData) {
                if (respData.loggedIn === true)  $state.go('home');
            });

        $scope.submit = function () {
            $log.debug($scope.form);
            AuthService.register($scope.form)
                .then(function (respData) {
                    if (respData.registered === true) {
                        $state.go('login');
                    }
                    if (respData.err) {
                        $scope.error = respData.err;
                    }

                });
        };
    }]);
