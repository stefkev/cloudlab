angular.module('app')
    .controller('Login', ['SessionService', '$scope',
        'AuthService', '$state', '$log', function (SessionService, $scope, AuthService, $state, $log) {
        AuthService.isLoggedIn()
            .then(function (respData) {
                if (respData.loggedIn == true) $state.go('home');
            });
        $scope.submit = function () {
            AuthService.login($scope.form.email, $scope.form.password)
                .then(function (respData) {
                    if (respData.loggedIn === true) {
                        $scope.$emit('getLoggedIn');
                        return $state.go('home');
                    }
                    $scope.error = 'Invalid username or password';
                });
        };
        }]);

