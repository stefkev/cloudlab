angular.module('app')
    .controller('User', ['$scope', '$http', 'SessionService', 'AuthService', '$state',
        '$log', '$timeout', function ($scope, $http, SessionService, AuthService, $state, $log, $timeout) {

            $scope.data = {};
            $scope.loggedIn = false;

            $scope.$emit("getLoggedIn");

            $scope.$on("loggedIn", function (event, data) {
                if(data.loggedIn == true){
                    AuthService.getUser()
                        .then(function (respData) {
                            $scope.$applyAsync(function () {
                                angular.copy(respData, $scope.data);
                            })
                        })
                }
                $scope.$applyAsync(function () {
                    $scope.loggedIn = angular.copy(data.loggedIn)
                })
            })
            $scope.logout = function () {
                AuthService.logout()
                    .then(function (respData) {
                        if (respData.loggedOut == true) {
                            $scope.$applyAsync(function () {
                                $scope.$emit('getLoggedIn');
                                $state.go('login')
                            })
                        }
                    })
            }
        }]);
