angular.module('app', ['ui.bootstrap', 'ui.router','ngFileUpload', 'ngMessages'])
    .config(function ($locationProvider, $stateProvider) {
        $locationProvider.html5Mode(true);
        var states = [
            {
                name: 'login',
                url: '/login',
                controller: 'Login',
                templateUrl: '/js/app/templates/login.html'
            },
            {
                name: 'register',
                url: '/register',
                controller: 'Register',
                templateUrl: '/js/app/templates/register.html'
            },
            {
                name: 'home',
                url: '/',
                controller: 'Home',
                templateUrl: '/js/app/templates/home.html'
            },
            {
                name: 'trends',
                url: '/trends',
                component: 'trends',
                templateUrl: '/js/app/templates/trends.html'
            },
            {
                name: 'editor',
                url: '/editor/:projectID',
                controller: 'Editor',
                templateUrl: '/js/app/templates/editor.html'
            },
            {
                name: 'user',
                url: '/user',
                templateUrl: '/js/app/templates/user.html'
            }
        ];
        states.forEach(function (state) {
            $stateProvider.state(state);
        });
    }).run(function () {
});
