(function() {
  'use strict';

  angular
      .module('app')
      .controller('HomeController', HomeController);

  HomeController.$inject = ['$scope', '$http', '$route', '$log'];

  function HomeController($scope, $http, $route, $log) {
    $scope.loggedIn = false;
    $scope.$watch('loggedIn', function(oldval, newval){
      return $scope.$digest;
    });
      $http.get('/users').success(function (response) {
          $scope.firstName = response.firstName;
          $scope.loggedIn = true;
          $scope.logout = function () {
              $scope.loggedIn = false;
          };
      });
  }
}());
