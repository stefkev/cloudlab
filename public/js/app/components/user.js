
function UserController() {

}
angular.module('app')
.component('userDetail',{
    templateUrl: '/js/app/templates/user.html',
    controller: UserController,
    bindings: {
        user: '='
    }
});
