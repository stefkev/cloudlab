angular.module('app')
    .directive('player', function() {
        return {
            restrict: 'E',
            templateUrl: "/js/app/templates/player.html",
            scope: {
                projectData: '='
            },
            controller: function($scope, $state,  $log, $document) {
                $scope.artist = "";
                $scope.projectName = "";
                $scope.projectCreated = "";
                $scope.projectID = "";
                $scope.playing = false;
                $scope.liked = false;
                $scope.likeButtonToggled = false;
                $scope.playPauseButton = "/images/play_button_up.png";
                $scope.likeButton = "/images/like_button_up.png";
                $scope.likes = 0;
                $scope.views = 0;
                $scope.wavesurfer = WaveSurfer.create({
                    container: '[property=test]',
                    progressColor: '#ff9900',
                    waveColor: '#eae09b',
                    cursorColor: '#a36125'
                });
                $log.debug($scope.projectData);
                // $scope.wavesurfer.load();
                $scope.wavesurfer.on('finish', function() {
                    $scope.playing = false;
                    $scope.playPauseButton = "/images/play_button_up.png";
                });
                $scope.edit = function(){
                    $state.go('editor', {projectID: $scope.projectData._id});
                };
                $scope.play = function() {
                    if ($scope.playing == false) {
                        $scope.wavesurfer.play();
                        $scope.playing = true;
                        $scope.playPauseButton = "/images/pause_button.png";
                    } else {
                        $scope.wavesurfer.pause();
                        $scope.playing = false;
                        $scope.playPauseButton = "/images/play_button_up.png";
                    }
                };
                $scope.likeHover = function() {
                    if ($scope.liked === false) {
                        $scope.likeButton = "/images/like_button_down.png";
                    }
                };
                $scope.likeUnhover = function() {
                    if ($scope.liked === false) {
                        $scope.likeButton = "/images/like_button_up.png";
                    }
                };
                $scope.like = function() {
                    if ($scope.liked == true) {
                        //like function
                        $scope.likeButton = "/images/like_button_up.png";
                        $scope.liked = false;
                    } else {
                        $scope.likeButton = "/images/like_button_down.png";
                        $scope.liked = true;
                    }
                };
                $scope.mouseover = function() {
                    if ($scope.playing == false) {
                        $scope.playPauseButton = "/images/play_button_down.png";
                    } else {
                        $scope.playPauseButton = "/images/pause_button_down.png";
                    }
                };
                $scope.mouseleave = function() {
                    if ($scope.playing == false) {
                        $scope.playPauseButton = "/images/play_button_up.png";
                    } else {
                        $scope.playPauseButton = "/images/pause_button.png";
                    }
                };

            }
        };
    });
