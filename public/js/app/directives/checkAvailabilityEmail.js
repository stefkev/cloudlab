angular.module('app')
    .directive('checkAvailabilityEmail', function ($q, AuthService) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                ctrl.$asyncValidators.checkAvailabilityEmail = function (modelValue, viewValue) {
                    if (ctrl.$isEmpty(modelValue)) {
                        // consider empty model valid
                        return $q.resolve();
                    }
                    var def = $q.defer();

                    AuthService.checkEmail(modelValue)
                        .then(function (respData) {
                            if (respData.exists == true) {
                                def.reject();
                            } else {
                                def.resolve();
                            }
                        });
                }
            }
        }
    })
