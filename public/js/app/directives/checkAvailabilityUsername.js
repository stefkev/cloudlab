angular.module('app')
.directive('checkAvailabilityUsername', function ($log, $q, AuthService) {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$asyncValidators.checkAvailabilityUsername = function (modelValue, viewValue) {
                if (ctrl.$isEmpty(modelValue)) {
                    return $q.resolve();
                }
                var def = $q.defer();

            AuthService.checkUsername(modelValue)
                .then(function (respData) {
                    if(respData.exists == true){
                        def.reject();
                    }else{
                        def.resolve();
                    }
                })
                return def.promise
            }
        }
    }
})