var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var stemSchema = new Schema({
    name: {type: String},
    refID: {type: Schema.ObjectId}

});

var Stem = mongoose.model('Stem', stemSchema);

module.exports = {
    Stem: Stem
};